# Contributing

Primarily this is used to exercise the own skills. Yet, hints on how to improve
coding or fixing some errors are welcome. Simply open an pull request.

Contributions
- may not break any tests
- shall adhere to the coding style


## Commit messages
- should adhere to conventional commits
- so they are like

```
feat(scope)!: Headline

A more detailed description

BREAKING CHANGE: whatever breaks
```

Types are:
- `feat` (new features)
- `fix` (bugfixes)
- `chore` (changes that are no fixes or features and
  don't modify src or test files)
- `refactor` (refactored code without new features or fixes)
- `docs` (updating documentation)
- `style` (not affecting meaning of the code)
- `test` (new or correcting tests)
- `perf` (performance improvements)
- `ci` (relating to continuous integration)
- `build` (changes affecting the build system)
- `revert` (reverting a previous commit)

Scopes should be given, if possible

The `!` has to follow in case of breaking changes. The headline summarizes the
changes in one sentence.

The description might span multiple lines or paragraphs. Paragraphs may be 
seperated by empty lines.

The footer has to contain
```
BREAKING CHANGE: description of whatever breaks
```
in case of an indicated breaking change after the type.

The footer may contain more lines like
```
some-topic: details
```

General rules are the [Conventional Commits Guide](https://www.conventionalcommits.org/en/v1.0.0/)
