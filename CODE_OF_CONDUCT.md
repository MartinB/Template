# Code of Conduct

--- EXAMPLE!!! ---

Please see
[Contributor Covenant CoC](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html) and the [Kabuki Toolkit™ Cookbook](https://github.com/KabukiStarship/kabuki.toolkit.cookbook). 
You have to agree with the vollowing documents in order to contribute to this 
repository:

* [Change Control Procedure]
* [Code of Conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html)
* [Secure Coding Standards]

