
# For every release

- Review Changelog and NEWS
- git status - Is everything checked in?
- Do all tests run without errors?
- Does the linter have any remarks?
- Add the tag in git
- And git push it...

