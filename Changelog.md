# Changelog

## unreleased

### ADDED

### CHANGED
### DEPRECATED
### REMOVED
### FIXED
### SECURITY

### Known Issues

## 0.0.2 - released 2022-12-18

### ADDED
- Code of Conduct
- NEWS
- Roadmap
- Templates for issues, feature requests, bugreports 

## 0.0.1 - released 2022-12-18

### ADDED
- Default README
- Some hints on using template
