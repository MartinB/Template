# <Project>

## About

This is a template for new projects. Default Changelog included. 
License still to be chosen. README with default sections.

## Build status and Test status badge

## Introduction

## Status of project

## Installation

## Requirements

## Getting Started

## Documentation

## Support

## License

## Changelog

Detailed information about changes can be found in the file [Changelog](Changelog.md). 

## Contributing

For more information see [Contributing.md](CONTRIBUTING.md).

## Getting in touch

You can contact me by [mail](mailto:martin.brunzel@gmx.de).

## Library version badge
